﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCreator : MonoBehaviour 
{
	private float minXvalue = -1.9f;
	private float maxXvalue = 2.2f;

	private float minYvalue = 3.5f;
	private float maxYvalue = -0.02f;

	private float xSpace = 0.889f;
	private float ySpace = -0.26f;

	private float currentXValue = -1.9f;
	private float currentYValue = 3.5f;

	private int noOfBricks = 0;
	public GameObject brick;

	public int CreateLevel(List<string> lines)
	{
		if (lines == null) 
		{
			return -1;
		}

		noOfBricks = 0;
		currentXValue = minXvalue;
		currentYValue = minYvalue;

		for(int k = 0; k < lines.Count; k++)
		{
			string line = lines [k];
			for (int i = 0; i < line.Length; i++) 
			{
				switch (line[i]) 
				{
				case LevelCreatorConstant.SingleBrick:
					{
						GameObject newBrick = Instantiate(brick,transform) as GameObject;

						newBrick.transform.position = new Vector3 (currentXValue,currentYValue,0f);
						newBrick.AddComponent<SingleBrick> ();
						newBrick.SetActive (true);

						noOfBricks++;
					}
					break;
				case LevelCreatorConstant.DoubleBrick:
					{
						GameObject newBrick = Instantiate(brick,transform) as GameObject;

						newBrick.transform.position = new Vector3 (currentXValue,currentYValue,0f);
						newBrick.AddComponent<DoubleBrick> ();
						newBrick.SetActive (true);

						noOfBricks++;
					}
					break;
				case LevelCreatorConstant.Astrix:
					{
					}
					break;
				case LevelCreatorConstant.Space:
					{
						currentXValue -= xSpace/2;
					}
					break;
				default:
					break;
				}

				currentXValue += xSpace;

				if (currentXValue > maxXvalue) 
				{
					break;
				}

			}
			currentYValue += ySpace;
			currentXValue = minXvalue;

			if (currentYValue < maxYvalue) 
			{
				break;
			}
		}

		return noOfBricks;
	}

}
