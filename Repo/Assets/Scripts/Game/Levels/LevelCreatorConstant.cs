﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCreatorConstant
{
	public const char SingleBrick = '1';
	public const char DoubleBrick = '2';
	public const char Space = ' ';
	public const char Astrix = '*';
}
