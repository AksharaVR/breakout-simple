﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBall : Ball {

	private Rigidbody rb;
	private bool ballInPlay;

	void Awake () 
	{
		ballInitialVelocity = 300f;

		rb = transform.GetComponent<Rigidbody> ();


		GameActions.OnGameStart += StartBall;
		GameActions.OnNewLevel += ResetBall;
	}

	void FixedUpdate () 
	{
		#if UNITY_EDITOR

		if ((Input.GetKeyDown(KeyCode.Space)) && ballInPlay == false)
		{
			AddInitialForce ();
		}
		#endif
	}

	private void StartBall()
	{
		if (ballInPlay == false) 
		{
			AddInitialForce ();
		}
	}

	private void ResetBall()
	{
		DestroyBall ();
	}

	protected override void AddInitialForce()
	{
		transform.parent = null;
		ballInPlay = true;
		rb.isKinematic = false;
		rb.AddForce(new Vector3(ballInitialVelocity, ballInitialVelocity, 0));
	}


	void OnDisable()
	{
		GameActions.OnGameStart -= StartBall;
		GameActions.OnNewLevel -= ResetBall;
	}

}