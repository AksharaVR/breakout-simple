﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ball : MonoBehaviour 
{

	protected float ballInitialVelocity;

	protected abstract void AddInitialForce ();

	public void DestroyBall()
	{
		Destroy (gameObject);
	}

}
