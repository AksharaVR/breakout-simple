﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	private bool isGameStarted = false;
	public bool IsGameStarted
	{
		get
		{
			return isGameStarted;
		}
		set
		{
			isGameStarted = value;
			if (isGameStarted) 
			{
				GameActions.StartGame ();
			}
		}
	}

	private int lives = 3;
	private int score = 0;
	private int currentLevel = 1;

	private float resetDelay = 1f;

	public int totalLevels = 20;

	public GameObject paddle;
	private GameObject clonePaddle;
	private GameObject cloneBrick;
	public static GameManager instance = null;

	private int bricks = 16;
	public GameObject bricksPrefab;
	public SimpleTouchPad touchPad;

	// Use this for initialization
	void Awake () 
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		currentLevel = 1;

		Setup();

	}

	public void Setup()
	{

		Reset ();

		SetupGame ();

		SetupLevel ();
	}

	void CheckGameOver()
	{
		if ((bricks < 1) && (currentLevel == totalLevels)) 
		{
			UIManager.Instance.ShowYouWinPanel (score);
			Time.timeScale = .25f;
		}
		else if ((bricks < 1) && (currentLevel < totalLevels)) 
		{
			currentLevel++;

			Destroy(clonePaddle);
			Invoke ("SetupGame", resetDelay);
			Destroy(cloneBrick);
			Invoke ("SetupLevel", resetDelay);
			GameActions.NewLevel();
		}

		if (lives < 1)
		{
			UIManager.Instance.ShowGameOverPanel (score);
			Time.timeScale = .25f;
		}

	}

	void Reset()
	{
		Time.timeScale = 1f;
		score = 0;
	}

	public void LoseLife()
	{
		lives--;
		UIManager.Instance.SetLife (lives);
		Destroy(clonePaddle);
		Invoke ("SetupGame", resetDelay);
		CheckGameOver();
	}

	void SetupGame()
	{
		UIManager.Instance.SetScore (score);
		IsGameStarted = false;
		clonePaddle = Instantiate(paddle) as GameObject;
	}

	void SetupLevel()
	{
		cloneBrick = Instantiate(bricksPrefab, transform.position, Quaternion.identity) as GameObject;
		bricks = cloneBrick.GetComponent<LevelCreator> ().CreateLevel (FileReader.ReadFile(currentLevel));

		if (bricks == -1) 
		{
			currentLevel = totalLevels;
			CheckGameOver ();
		} 
		else if (currentLevel >= 1) 
		{
			UIManager.Instance.ShowNextLevelPanel (currentLevel);
		}
	}

	public void DestroyBrick(int brickScore, bool reduceBrick)
	{		
		score += brickScore;
		UIManager.Instance.SetScore (score);

		if (reduceBrick) 
		{
			bricks--;
			CheckGameOver ();
		}
	}
}
