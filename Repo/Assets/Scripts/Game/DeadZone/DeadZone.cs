﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour {

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.GetComponent<Ball> () != null)
		{
			col.gameObject.GetComponent<Ball> ().DestroyBall ();
		}

		GameManager.instance.LoseLife();
	}
}