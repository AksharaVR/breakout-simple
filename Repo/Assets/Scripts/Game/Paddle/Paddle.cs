﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Paddle : MonoBehaviour 
{

	protected float paddleSpeed;

	protected float xPos;
	protected float minXValue;
	protected float maxXValue;
	protected float yPos;

}
