﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalPaddle : Paddle 
{
	private Vector3 playerPos;

	void Awake()
	{
		paddleSpeed = 0.1f;
		yPos = -4.25f;

		minXValue = -1.7f;
		maxXValue = 1.7f;
	}

	void FixedUpdate () 
	{
		#if UNITY_EDITOR

		xPos = transform.position.x + (Input.GetAxis("Horizontal") * paddleSpeed);

		#else

		if (GameManager.instance.touchPad.GetPosition () != Vector2.zero)
		{
			xPos = (Camera.main.ScreenToWorldPoint (new Vector3 (GameManager.instance.touchPad.GetPosition ().x, GameManager.instance.touchPad.GetPosition ().y, 0f))).x;
		} 
		else 
		{
			xPos = transform.position.x;
		}

		#endif

		playerPos = new Vector3 (Mathf.Clamp (xPos, minXValue,maxXValue), yPos, 0f);
		transform.position = playerPos;

	}
}
