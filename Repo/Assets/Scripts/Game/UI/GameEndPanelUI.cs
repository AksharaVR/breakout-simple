﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEndPanelUI : MonoBehaviour 
{
	public Text headingTxt;
	public Text scoreTxt;
	public Button retryBtn;
	public Button homeBtn;

	void OnEnable() 
	{
		retryBtn.onClick.AddListener (OnRetryBtnClicked);
		homeBtn.onClick.AddListener (OnHomeBtnClicked);
	}	

	public void SetText (string heading, int score)
	{
		headingTxt.text = heading;
		scoreTxt.text = "Score : "+score.ToString ();
	}

	private void OnRetryBtnClicked()
	{
		DisablePanel ();
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
//		Application.LoadLevel(Application.loadedLevel);
	}

	private void OnHomeBtnClicked()
	{
		DisablePanel ();
		ApplicationManager.Instance.LoadScene (ApplicationConstant.MainScene);
	}


	void DisablePanel ()
	{
		Destroy (gameObject);
	}

	void OnDisable()
	{
		retryBtn.onClick.RemoveListener (OnRetryBtnClicked);
		homeBtn.onClick.RemoveListener (OnHomeBtnClicked);
	}
}
