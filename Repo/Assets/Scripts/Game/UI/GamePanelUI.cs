﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePanelUI : Panel 
{

	public Button backBtn;
	public Text livesTxt;
	public Text scoreTxt;

	public override void OnEnable() 
	{ 
		base.OnEnable ();

		scoreTxt.text = "Score : 0";
		backBtn.onClick.AddListener (OnBackBtnClicked);
	}	

	private void OnBackBtnClicked()
	{
		UIManager.Instance.ShowMainMenuPanel();
	}

	public void SetLife(int lives)
	{
		livesTxt.text = "Lives : "+lives.ToString();
	}

	public void SetScore(int score)
	{
		scoreTxt.text = "Score : "+score.ToString();
	}

	void OnApplicationQuit()
	{
		Destroy (gameObject);
	}

	public override void OnDisable()
	{
		base.OnDisable ();
		backBtn.onClick.RemoveListener (OnBackBtnClicked);
	}
}
