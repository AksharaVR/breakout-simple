﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLevelPanelUI : MonoBehaviour 
{
	public Text levelTxt;

	void OnEnable()
	{
		Invoke ("DisablePanel", 1.5f);
	}

	public void SetText (int level)
	{
		levelTxt.text = "Level : "+level.ToString ();
	}

	void DisablePanel ()
	{
		Destroy (gameObject);
	}
}
