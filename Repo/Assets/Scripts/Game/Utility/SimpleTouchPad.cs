﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SimpleTouchPad : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {

	private Vector2 currentPosition;
	private bool touched;
	private int pointerID;

	void Awake () {
		touched = false;
	}

	public void OnPointerDown (PointerEventData data) {
		if (!touched) {
			touched = true;
			pointerID = data.pointerId;

//			if (!GameManager.instance.IsGameStarted) 
//			{
//				GameManager.instance.IsGameStarted = true;
//			}
		}
	}

	public void OnDrag (PointerEventData data) {
		if (data.pointerId == pointerID) {
			currentPosition = data.position;
		}
	}

	public void OnPointerUp (PointerEventData data) {
		if (data.pointerId == pointerID) {
			currentPosition = Vector2.zero;
			touched = false;

			if (!GameManager.instance.IsGameStarted) 
			{
				GameManager.instance.IsGameStarted = true;
			}
		}
	}

	public Vector2 GetPosition()
	{
		return currentPosition;
	}

}