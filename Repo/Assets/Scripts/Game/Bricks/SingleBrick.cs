﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleBrick : Brick 
{
		void Awake()
		{
			brickValue = 10;
			hit = 1;
		GetComponent<MeshRenderer> ().material = GetComponent<BrickData> ().brickMaterial [hit - 1];
		}

		void OnCollisionEnter (Collision other)
		{
			hit--;

			if (hit <= 0) 
			{
				GameManager.instance.DestroyBrick (brickValue, true);
				Destroy (gameObject);
			} 
			else 
			{
				GetComponent<MeshRenderer> ().material = GetComponent<BrickData> ().brickMaterial [hit - 1];

				GameManager.instance.DestroyBrick (brickValue, false);
			}
		}
}