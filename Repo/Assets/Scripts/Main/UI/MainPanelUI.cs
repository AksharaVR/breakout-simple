﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPanelUI : MonoBehaviour 
{

	public Button playBtn;
	public Button quitBtn;

	void OnEnable() 
	{
		playBtn.onClick.AddListener (OnPlayBtnClicked);
		quitBtn.onClick.AddListener (OnQuitBtnClicked);
	}	

	private void OnPlayBtnClicked()
	{
		ApplicationManager.Instance.LoadScene (ApplicationConstant.GameScene);
	}

	private void OnQuitBtnClicked()
	{
		UIManager.Instance.ShowApplicationQuitPanel ();
	}


	void OnDisable()
	{
		playBtn.onClick.RemoveListener (OnPlayBtnClicked);
		quitBtn.onClick.RemoveListener (OnQuitBtnClicked);
	}
}
