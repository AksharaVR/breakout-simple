﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpPanelUI : MonoBehaviour 
{
	public Text headingTxt;
	public Button yesBtn;
	public Button noBtn;

	void OnEnable() 
	{

	}	

	public void SetPopup (string text,string type)
	{
		headingTxt.text = text;

		if (type == ApplicationConstant.Popup_Back)
		{
			yesBtn.onClick.AddListener (OnBackYesBtnClicked);
			noBtn.onClick.AddListener (OnCancelClicked);
		}

		else if (type == ApplicationConstant.Popup_Quit)
		{
			yesBtn.onClick.AddListener (OnQuitYesBtnClicked);
			noBtn.onClick.AddListener (OnCancelClicked);
		}
	}

	private void OnCancelClicked()
	{
		DisablePanel ();
	}

	private void OnQuitYesBtnClicked()
	{
		DisablePanel ();
		ApplicationManager.Instance.CloseApplication ();
	}

	private void OnBackYesBtnClicked()
	{
		DisablePanel ();
		ApplicationManager.Instance.LoadScene (ApplicationConstant.MainScene);
	}


	void DisablePanel ()
	{
		Destroy (gameObject);
	}

	void OnDisable()
	{
		yesBtn.onClick.RemoveListener(OnQuitYesBtnClicked);
		yesBtn.onClick.RemoveListener(OnBackYesBtnClicked);
		noBtn.onClick.RemoveListener (OnCancelClicked);
	}
}
