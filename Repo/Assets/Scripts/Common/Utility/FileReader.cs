﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class FileReader
{
	public static List<string>  ReadFile(int level)
	{
		List<string> levelLines = new List<string> ();

		string filePath = "Levels/level"+level.ToString();
		TextAsset file = Resources.Load(filePath) as TextAsset;  

		if (file == null) 
		{
			return null;
		}
			string replaceString = file.text.Replace("\r\n", "\n");
			string[] lines = replaceString.Split ("\n" [0]);

		for (int i = 0; i < lines.Length; i++)
		{
			levelLines.Add (lines [i]);
		}

		return levelLines;
	
	}
}
