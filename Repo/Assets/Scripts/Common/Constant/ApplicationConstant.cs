﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationConstant
{
	public const string MainScene = "MainScene";
	public const string GameScene = "GameScene";


	public const string Popup_Back = "Popup_Back";
	public const string Popup_Quit = "Popup_Quit";

}

public class PanelConstant
{
	public const string Panel_GamePanel = "GamePanelUI";
}
