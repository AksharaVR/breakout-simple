﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameActions 
{
	public static event Action OnGameStart;
	public static event Action OnNewLevel;

	public static void StartGame()
	{
		if (GameActions.OnGameStart != null) 
		{
			GameActions.OnGameStart ();
		}
	}

	public static void NewLevel()
	{
		if (GameActions.OnNewLevel != null) 
		{
			GameActions.OnNewLevel ();
		}
	}
} 
