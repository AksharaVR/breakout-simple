﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : GenericSingletonClass<UIManager> 
{
	public GameObject popUpPanel;
	public GameObject gameEndPanel;
	public GameObject gameLevelPanel;

	public List<Panel> gamePanelList;

	public void ShowApplicationQuitPanel()
	{
		GameObject popUpPanelObject = Instantiate(popUpPanel,GameObject.Find("Canvas").transform) as GameObject;

		if (popUpPanelObject.GetComponent<PopUpPanelUI>() != null) 
		{
			popUpPanelObject.GetComponent<PopUpPanelUI> ().SetPopup ("Exit?",ApplicationConstant.Popup_Quit);
		}
	}

	public void ShowMainMenuPanel()
	{
		GameObject popUpPanelObject = Instantiate(popUpPanel,GameObject.Find("Canvas").transform) as GameObject;

		if (popUpPanelObject.GetComponent<PopUpPanelUI>() != null) 
		{
			popUpPanelObject.GetComponent<PopUpPanelUI> ().SetPopup ("Back to Main Menu",ApplicationConstant.Popup_Back);
		}
	}


	public void ShowGameOverPanel(int score)
	{
		GameObject gameEndPanelObject = Instantiate(gameEndPanel,GameObject.Find("Canvas").transform) as GameObject;

		if (gameEndPanelObject.GetComponent<GameEndPanelUI>() != null) 
		{
			gameEndPanelObject.GetComponent<GameEndPanelUI> ().SetText ("Game Over!",score);
		}
	}

	public void ShowYouWinPanel(int score)
	{
		GameObject gameEndPanelObject = Instantiate(gameEndPanel,GameObject.Find("Canvas").transform) as GameObject;

		if (gameEndPanelObject.GetComponent<GameEndPanelUI>() != null) 
		{
			gameEndPanelObject.GetComponent<GameEndPanelUI> ().SetText ("You Win!",score);
		}
	}

	public void ShowNextLevelPanel(int level)
	{
		GameObject gameLevelPanelObject = Instantiate(gameLevelPanel,GameObject.Find("Canvas").transform) as GameObject;

		if (gameLevelPanelObject.GetComponent<GameLevelPanelUI>() != null) 
		{
			gameLevelPanelObject.GetComponent<GameLevelPanelUI> ().SetText (level);
		}
	}


	public T GetPanel<T>() where T : Panel
	{
		for (int i = 0; i < gamePanelList.Count; i++) 
		{
			System.Type type = typeof(T);

			if (type.ToString () == (gamePanelList [i].name)) 
			{
				return (T)(gamePanelList [i]);
			}
		}
		return null;
	}

	public void SetLife(int lives)
	{
		if (GetPanel<GamePanelUI> () != null) {
			GetPanel<GamePanelUI> ().SetLife (lives);
		}
	}

	public void SetScore(int score)
	{
		if (GetPanel<GamePanelUI> () != null) {
			GetPanel<GamePanelUI> ().SetScore (score);
		}
	}
}