﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationManager : GenericSingletonClass<ApplicationManager> 
{
	public override void Awake()
	{
		base.Awake ();

		transform.parent = MainManager.Instance.gameObject.transform;
	}

	public void LoadScene(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}

	public void CloseApplication()
	{
		Application.Quit ();
	}
}
