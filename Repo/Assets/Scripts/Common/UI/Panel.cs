﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : MonoBehaviour 
{
	public virtual void OnEnable()
	{
		UIManager.Instance.gamePanelList.Add (this);
	}

	public virtual void OnDisable()
	{
		if (UIManager.Instance.gamePanelList != null) 
		{
			UIManager.Instance.gamePanelList.Remove (this);
		}
	}
}
